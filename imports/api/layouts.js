import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Layouts = new Mongo.Collection('layouts');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('layouts', function layoutsPublication() {
    return Layouts.find({});
  });
}
