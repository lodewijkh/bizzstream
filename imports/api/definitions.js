import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Definitions = new Mongo.Collection('definitions');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('definitions', function definitionsPublication() {
    return Definitions.find({});
  });
}
