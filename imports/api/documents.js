import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Documents = new Mongo.Collection('documents');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('documents', function documentsPublication() {
    return Documents.find({});
  });
}

Meteor.methods({
  'documents.insert'(data) {
    data.age = Number( data.age );

    check(data.name, String);
    check(data.age, Number);

    Documents.insert(data);
  },
});
