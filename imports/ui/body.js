import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import { Definitions } from '../api/definitions.js';
import { Layouts } from '../api/layouts.js';
import { Documents } from '../api/documents.js';

import './body.html';

Template.body.onCreated(function bodyOnCreated() {
  Meteor.subscribe('definitions');
  Meteor.subscribe('layouts');
  Meteor.subscribe('documents');
});

Template.body.helpers({
  tasks() {
    return Layouts.find({});
  },
  theView(definition = Definitions.findOne({}), layout = Layouts.findOne({})) {
    const view = layout;

    if (view) {
      view.header.rows.forEach(function(row) {
        row.columns.forEach(function(column) {
          const col = column;
          definition.schema.fields.forEach(function(field) {
            if (field._id === column.fieldId) {
              Object.assign(col, field);
              delete col.fieldId;
            }
          });
        });
      });
    }
    return view;
  },
});

Template.body.events({
  'submit .save-document'(event) {
    let saveData = {};
    const formData = $( event.target ).serializeArray();

    // Prevent default browser form submit
    event.preventDefault();

    formData.forEach(function(field) {
      saveData[field.name] = field.value;
    });

    // Insert a document into the collection
    Meteor.call('documents.insert', saveData);
  },
});
