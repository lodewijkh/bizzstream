import { Meteor } from 'meteor/meteor';
import '../imports/api/definitions.js';
import '../imports/api/layouts.js';
import '../imports/api/documents.js';

import { Definitions } from '../imports/api/definitions.js';
import { Layouts } from  '../imports/api/layouts.js';
import { mockDefinition } from '../imports/mock/definition.js';
import { mockLayout } from  '../imports/mock/layout.js';

Meteor.startup(() => {
  Definitions.remove({});
  Definitions.insert(mockDefinition);
  Layouts.remove({});
  Layouts.insert(mockLayout);
});
